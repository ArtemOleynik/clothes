package com.service;

import com.entity.SaleEntity;

import java.util.List;

public interface SaleService {
    List<SaleEntity> getAllData();
    List<SaleEntity> getAllDataForBrand(String brandName);
    List<SaleEntity> getAllDataForBrandAndCategory(String brandName, String category);
    SaleEntity getSaleEntity(SaleEntity saleEntity);
    void update(SaleEntity saleEntity);
    void add(SaleEntity saleEntity);
    void remove(SaleEntity saleEntity);
    void removeAll();
    boolean isValid(SaleEntity saleEntity);
}
