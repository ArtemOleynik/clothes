package com.service.impl;

import com.dao.SaleDao;
import com.entity.SaleEntity;
import com.service.SaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SaleServiceImpl implements SaleService {

    private SaleDao saleDaoImpl;

    @Autowired
    public SaleServiceImpl(SaleDao saleDaoImpl) {
        this.saleDaoImpl = saleDaoImpl;
    }

    @Override
    public List<SaleEntity> getAllData() {
        return saleDaoImpl.getAllData();
    }

    @Override
    public List<SaleEntity> getAllDataForBrand(String brandName) {
        return saleDaoImpl.getAllDataForBrand(brandName);
    }

    @Override
    public List<SaleEntity> getAllDataForBrandAndCategory(String brandName, String category) {
        return saleDaoImpl.getAllDataForBrandAndCategory(brandName, category);
    }

    @Override
    public SaleEntity getSaleEntity(SaleEntity saleEntity) {
        return saleDaoImpl.getSaleEntity(saleEntity);
    }

    @Override
    public void update(SaleEntity saleEntity) {
        saleDaoImpl.update(saleEntity);
    }

    @Override
    public void add(SaleEntity saleEntity) {
        saleDaoImpl.add(saleEntity);
    }

    @Override
    public void remove(SaleEntity saleEntity) {
        saleDaoImpl.remove(saleEntity);
    }

    @Override
    public void removeAll() {
        saleDaoImpl.removeAll();
    }

    @Override
    public boolean isValid(SaleEntity saleEntity) {
        return saleEntity.getBrandName() != null
                && saleEntity.getCategory() != null
                && saleEntity.getClothesName() != null
                && saleEntity.getNewCost() != null
                && saleEntity.getOldCost() != null
                && saleEntity.getImg() != null
                && saleEntity.getUrlToClothes() != null;
    }
}
