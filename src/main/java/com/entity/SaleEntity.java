package com.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "sale")
public class SaleEntity implements Serializable{

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "img")
    private String img;

    @Column(name = "clothes_name")
    private String clothesName;

    @Column(name = "old_cost")
    private String oldCost;

    @Column(name = "new_cost")
    private String newCost;

    @Column(name = "url_to_clothes")
    private String urlToClothes;

    @Column(name = "category")
    private String category;

    @Column(name = "brand_name")
    private String brandName;
}
