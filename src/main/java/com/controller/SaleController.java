package com.controller;

import com.entity.SaleEntity;
import com.service.SaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SaleController {

    private SaleService saleServiceImpl;

    @Autowired
    public SaleController(SaleService saleServiceImpl) {
        this.saleServiceImpl = saleServiceImpl;
    }

    @GetMapping(value = "/all/")
    public ResponseEntity<?> getAllClothes(){
        List<SaleEntity> allData = saleServiceImpl.getAllData();
        if(allData!=null){
            return new ResponseEntity<>(allData, HttpStatus.OK);
        }else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Data was not found");
        }
    }

    @GetMapping(value = "/brand/{brandName}")
    public ResponseEntity<?> getSaleData(@PathVariable String brandName){
        List<SaleEntity> dataForBrand = saleServiceImpl.getAllDataForBrand(brandName);
        if(dataForBrand!=null&& !dataForBrand.isEmpty()){
            return new ResponseEntity<>(dataForBrand, HttpStatus.OK);
        }else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(String.format("Incorrect path variable %s", brandName));
        }
    }

    @GetMapping(value = "/brand/{brandName}/category/{categoryName}")
    public ResponseEntity<?> getSaleData(@PathVariable String brandName, @PathVariable String categoryName){
        List<SaleEntity> dataForBrandAndCategory = saleServiceImpl
                .getAllDataForBrandAndCategory(brandName, categoryName);
        if(dataForBrandAndCategory!=null&& !dataForBrandAndCategory.isEmpty()){
            return new ResponseEntity<>(dataForBrandAndCategory, HttpStatus.OK);
        }else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(String.format("Incorrect path variable %s or %s", brandName, categoryName));
        }
    }

    @PostMapping(value = "/clothes/add/")
    public ResponseEntity<?> addSaleData(@RequestBody SaleEntity saleEntity){
        if(saleServiceImpl.isValid(saleEntity)){
            saleServiceImpl.add(saleEntity);
            return ResponseEntity.status(HttpStatus.OK).build();
        }else {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
        }
    }

    @PutMapping(value = "/clothes/update/")
    public ResponseEntity<?> updateSaleData(@RequestBody SaleEntity saleEntity){
        if(saleServiceImpl.isValid(saleEntity)){
            saleServiceImpl.update(saleEntity);
            return ResponseEntity.status(HttpStatus.OK).build();
        }else {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
        }
    }

    @DeleteMapping(value = "/clothes/deleteAll/")
    public ResponseEntity<?> deleteAll() {
        if (!saleServiceImpl.getAllData().isEmpty()) {
            saleServiceImpl.removeAll();
            return ResponseEntity.status(HttpStatus.OK).build();
        } else {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
        }
    }

    @DeleteMapping(value = "/clothes/delete/")
    public ResponseEntity<?> deleteDelete(@RequestBody SaleEntity saleEntity) {
        if (saleServiceImpl.getSaleEntity(saleEntity)!=null) {
            saleServiceImpl.remove(saleEntity);
            return ResponseEntity.status(HttpStatus.OK).build();
        } else {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
        }
    }
}
